use thiserror::Error;

/// An `Scm` provides a distinction between different source control versioning systems.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Scm {
    /// Stands for the plastic-scm versioning system.
    PlasticScm,
}

impl Scm {
    /// Returns the name of a marker that signifies the presence of a particular SCM.
    pub fn marker(&self) -> &'static str {
        match self {
            Scm::PlasticScm => ".plastic",
        }
    }
}

impl std::str::FromStr for Scm {
    type Err = Error;

    fn from_str(value: &str) -> Result<Scm, Self::Err> {
        match value.to_lowercase().as_str() {
            "plasticscm" => Ok(Scm::PlasticScm),
            _ => Err(Error::ParseScmError(value.to_string())),
        }
    }
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("The value {0} is not understood as a variant of the enum Scm")]
    ParseScmError(String),
}
