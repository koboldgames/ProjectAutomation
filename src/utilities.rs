use crate::build_target::BuildTarget;
use crate::project::v7::{Project, BuildTargetSpec};
use crate::project::v6::{Project as v6Project};
use crate::scm::Scm;
use crate::semver::SemVer;
use glob;
use serde_json;
use std::fs::read_dir;
use std::fs::File;
use std::io::BufReader;
use std::str::FromStr;
use std::path::{Path, PathBuf};
use thiserror::Error;
use anyhow::{Context, Result};

extern crate yaml_rust;
use yaml_rust::{YamlLoader};

#[derive(Debug, Error)]
pub enum Error {
    #[error("The {0} argument is missing")]
    ArgumentMissing(&'static str),
    #[error("The path {} does not point to a file", .0.display())]
    NotAFile(PathBuf),
    #[error("The path {} does not point to a directory", .0.display())]
    NotADirectory(PathBuf),
    #[error("Cannot canonicalize the path")]
    CanonicalizePathError(#[source] std::io::Error),
    #[error("Cannot open the file at {}", .0.display())]
    CannotOpenFile(PathBuf, #[source] std::io::Error),
    #[error("Cannot deserialize the file at {}", .0.display())]
    CannotDeserializeFile(PathBuf, #[source] serde_json::Error),
}

static FAST_UNITY_PATHS: &'static [&'static str] = &[
    #[cfg(target_os = "windows")]
    "C:\\Program Files (x86)\\Unity*\\Editor\\Unity.exe",
    #[cfg(target_os = "windows")]
    "C:\\Program Files (x86)\\Unity\\Hub\\Editor\\*\\Editor\\Unity.exe",
    #[cfg(target_os = "windows")]
    "C:\\Program Files\\Unity*\\Editor\\Unity.exe",
    #[cfg(target_os = "windows")]
    "C:\\Program Files\\Unity\\Hub\\Editor\\*\\Editor\\Unity.exe",
    #[cfg(target_os = "macos")]
    "/Applications/Unity*/Unity.app/Contents/MacOS/Unity",
    #[cfg(target_os = "macos")]
    "/Applications/Unity/Hub/Editor/*/Unity.app/Contents/MacOS/Unity",
    #[cfg(target_os = "linux")]
    "/opt/Unity/Editor/Unity",
];

static SLOW_UNITY_PATHS: &'static [&'static str] = &[
    #[cfg(target_os = "windows")]
    "C:\\**\\Editor\\Unity.exe",
    #[cfg(target_os = "macos")]
    "/Applications/**/Unity.app/Contents/MacOS/Unity",
    #[cfg(target_os = "linux")]
    "/home/**/Editor/Unity",
];

/// Recursively traverses a given path upwards and attempts to find the directory containing the
/// file or directory specified as SCM marker.
pub fn find_scm_root(scm: Scm, project: &Path) -> Option<PathBuf> {
    read_dir(project)
        .ok()?
        .filter_map(|r| {
            if let Ok(e) = r {
                if e.file_name() == scm.marker() {
                    Some(project.to_path_buf())
                } else {
                    None
                }
            } else {
                None
            }
        })
        .last()
        .or_else(|| project.parent().and_then(|p| find_scm_root(scm, p)))
}

/// Given a configuration file, load the Koboldgames project
pub fn load_project(cfg: &Path) -> Result<Project> {
    let file = File::open(&cfg).map_err(|e| Error::CannotOpenFile(cfg.to_path_buf(), e))?;
    let buf = BufReader::new(file);
    let project: v6Project = serde_json::from_reader(buf)
        .map_err(|e| Error::CannotDeserializeFile(cfg.to_path_buf(), e))?;

    return Ok(Project {
        name: project.name,
        unity_version: project.unity_version,
        version: project.version,
        build_targets: project.products[0].build_targets.iter().map(|target| BuildTargetSpec{
            // identifier: target.identifier.to_string(),
            target: target.target,
        }).collect::<Vec<_>>(),
    });
}

pub fn build_project(project: &Path) -> Result<Project> {
        let project_settings_string = std::fs::read_to_string(project.join("ProjectSettings/ProjectSettings.asset"))?;
        let project_settingss = YamlLoader::load_from_str(&project_settings_string)?;
        let project_settings = &project_settingss[0];

        let kg_project_settings_string = std::fs::read_to_string(project.join("Assets/KoboldgamesProjectSettings.asset"))?;
        let kg_project_settingss = YamlLoader::load_from_str(&kg_project_settings_string)?;
        let kg_project_settings = &kg_project_settingss[0]["MonoBehaviour"];

        let bundle_version = project_settings["PlayerSettings"]["bundleVersion"].as_str().unwrap();
        let ver = SemVer {
            major: bundle_version.split('.').take(1).last().and_then(|v| v.parse::<usize>().ok()).unwrap(),  // Option<str> --> Option<usize> --> usize oder panic,
            minor: bundle_version.split('.').take(2).last().and_then(|v| v.parse::<usize>().ok()).unwrap(),
            patch: bundle_version.split('.').take(3).last().and_then(|v| v.parse::<usize>().ok()).unwrap(),
            prerelease: String::new(),
            build_metadata: kg_project_settings["buildNumber"].as_i64().unwrap().to_string(),
        };

        let build_target_specs = kg_project_settings["buildTargets"].as_vec().unwrap().iter().map(|target| BuildTargetSpec{
            target: BuildTarget::from_str(target["target_string"].as_str().unwrap()).unwrap()
        }).collect::<Vec<_>>();

        return Ok(Project {
            name: project_settings["PlayerSettings"]["productName"].as_str().unwrap().to_string(),
            unity_version: kg_project_settings["unityVersion"].as_f64().unwrap().to_string(),
            version: ver,
            build_targets: build_target_specs,
        });
}

pub fn get_project(project: &Path) -> Result<Project> {
    if use_koboldgames_project_settings_asset(project) {
        return build_project(project).context("Cannot load the ProjectSettings.asset-project");
    } else {
        let project_json = project.join("Assets/StreamingAssets/project.json");
        return load_project(&project_json).context("Cannot load the project.json-project");
    }
}

/// Constructs the build path of a product.
pub fn product_path(
    project_name: &str,
    version: &SemVer,
    target: BuildTarget,
    build_path: &Path,
    project: &Path,
) -> PathBuf {

    let target_name = target.to_str().to_lowercase();
    let mut tmp;

    if use_koboldgames_project_settings_asset(project) {

        let output_dir_name = format!(
            "{}_{}_v{}",
            project_name,
            target_name,
            version.public()
        );

        if target == BuildTarget::WebGl {
            tmp = build_path
                .join(output_dir_name)
                .join(project_name.to_lowercase().replace(" ", "_"));
        } else {
            tmp = build_path
                    .join(output_dir_name)
                    .join(project_name);
        }

    } else {

        let output_dir_name = format!(
            "{}_{}_v{}",
            project_name.replace(" ", "_").to_lowercase(),
            target_name,
            version.public()
        );
        tmp = build_path
            .join(output_dir_name)
            .join("main")
            .join(project_name.replace(" ", ""));
    }

    if let Some(ext) = target.extension() {
        tmp = tmp.with_extension(ext);
    }

    tmp
}

/// Given an app name returns a normalized version
pub fn app_name(name: &str, project: &Path) -> String {
    if use_koboldgames_project_settings_asset(project) {
        return name.to_string();
    } else {
        return name.replace(" ", "");
    }
}

/// Attempts to find all installed unity editors on a system.
pub fn find_unity_editors() -> Vec<PathBuf> {
    let mut unity_editors: Vec<PathBuf> = FAST_UNITY_PATHS
        .iter()
        .filter_map(|search_path| glob::glob(search_path).ok())
        .flat_map(|paths_iter| paths_iter.filter_map(Result::ok))
        .collect();

    if unity_editors.is_empty() {
        unity_editors.append(
            &mut SLOW_UNITY_PATHS
                .iter()
                .filter_map(|search_path| glob::glob(search_path).ok())
                .flat_map(|paths_iter| paths_iter.filter_map(Result::ok))
                .collect(),
        );
    }

    unity_editors
}

/// returns true if KoboldgamesProjectSettings.asset exists
pub fn use_koboldgames_project_settings_asset(
     project: &Path
) -> bool {
    return project
        .join("Assets")
        .join("KoboldgamesProjectSettings.asset")
        .exists();
}
