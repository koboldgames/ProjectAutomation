use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
#[serde(into = "&'static str")]
pub enum ExportDestination {
    Export,
    Upload,
}

impl std::str::FromStr for ExportDestination {
    type Err = ();

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        use self::ExportDestination::*;
        match value.to_lowercase().as_str() {
            "export" => Ok(Export),
            "upload" => Ok(Upload),
            _ => Err(()),
        }
    }
}

impl Into<&'static str> for ExportDestination {
    fn into(self) -> &'static str {
        (&self).into()
    }
}

impl Into<&'static str> for &ExportDestination {
    fn into(self) -> &'static str {
        use self::ExportDestination::*;

        match self {
            Export => "export",
            Upload => "upload",
        }
    }
}

impl std::fmt::Display for ExportDestination {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", Into::<&'static str>::into(self))
    }
}
