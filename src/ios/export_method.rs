use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
#[serde(into = "&'static str")]
pub enum ExportMethod {
    AppStore,
    Validation,
    AdHoc,
    Package,
    Enterprise,
    Development,
    DeveloperId,
    MacApplication,
}

impl std::str::FromStr for ExportMethod {
    type Err = ();

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        use self::ExportMethod::*;
        match value.to_lowercase().as_str() {
            "app-store" => Ok(AppStore),
            "validation" => Ok(Validation),
            "ad-hoc" => Ok(AdHoc),
            "package" => Ok(Package),
            "enterprise" => Ok(Enterprise),
            "development" => Ok(Development),
            "developer-id" => Ok(DeveloperId),
            "mac-application" => Ok(MacApplication),
            _ => Err(()),
        }
    }
}

impl Into<&'static str> for ExportMethod {
    fn into(self) -> &'static str {
        (&self).into()
    }
}

impl Into<&'static str> for &ExportMethod {
    fn into(self) -> &'static str {
        use self::ExportMethod::*;

        match self {
            AppStore => "app-store",
            Validation => "validation",
            AdHoc => "ad-hoc",
            Package => "package",
            Enterprise => "enterprise",
            Development => "development",
            DeveloperId => "developer-id",
            MacApplication => "mac-application",
        }
    }
}

impl fmt::Display for ExportMethod {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", Into::<&'static str>::into(self))
    }
}
