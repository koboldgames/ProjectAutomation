//! This module contains version 6 of the project configuration specification.
use crate::build_target::BuildTarget;
use crate::semver::SemVer;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub enum BuildSteps {
    FullPipeline,
    BuildOnly,
    BuildAndVerify,
}

/// Specifies a build target / platform.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct BuildTargetSpec {
    pub target: BuildTarget
}

/// A `Project` defines settings that relate to a single Unity project.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Project {
    /// Defines the application name
    pub name: String,
    /// Each project will be built with a particular Unity version. When builds are made, this
    /// variable will be set to the appropriate Unity editor version.
    #[serde(default)]
    pub unity_version: String,
    /// Each project will itself have a version. Here, we follow the [SemVer](https://semver.org/)
    /// convention with a few minor adjustments. The public representation of the version is
    /// assigned to `PlayerSettings.bundleVersion`. An iOS-specific representation is assigned to
    /// `PlayerSettings.iOS.buildNumber`, and an Android-specific representation is assigned to
    /// `PlayerSettings.Android.bundleVersionCode`.
    #[serde(default)]
    pub version: SemVer,
    /// Defines the enabled build platforms / targets for this project.
    #[serde(default)]
    pub build_targets: Vec<BuildTargetSpec>,
}

impl Project {
    /// Returns the shortened unity version
    pub fn unity_version_short(&self) -> String {
        let parts = self.unity_version.split('.').take(2).collect::<Vec<_>>();

        format!("{}.{}", parts[0], parts[1])
    }

    pub fn unity_major_version(&self) -> usize {
        self.unity_version.split('.').take(1).last().and_then(|v| v.parse::<usize>().ok()).unwrap()  // Option<str> --> Option<usize> --> usize oder panic
    }

    pub fn unity_minor_version(&self) -> usize {
        self.unity_version.split('.').take(2).last().and_then(|v| v.parse::<usize>().ok()).unwrap()  // Option<str> --> Option<usize> --> usize oder panic
    }
}
