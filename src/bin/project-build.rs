use anyhow::{Context, Result};
use clap::{load_yaml, App};
use project_automation::{
    android::keystore_credentials::KeystoreCredentials,
    build_target::{BuildTarget, Error as BuildTargetError},
    build_type::Error as BuildTypeError,
    commands::{build_ios_xcarchive, build_unity, export_ios_ipa, sign_osx},
    ios::export_destination::ExportDestination,
    scm::{Error as ScmError, Scm},
    utilities::{find_scm_root, get_project, product_path, app_name},
};
use std::path::PathBuf;
use thiserror::Error;

static LEGACY_BM: &'static str = "Koboldgames.ContinuousIntegration.ContinuousIntegration.Build";
static KOBOLDTOOLS_BM: &'static str = "KoboldTools.Building.ContinuousIntegration.Build";

#[derive(Debug, Error)]
enum Error {
    #[error("The {0} argument is missing")]
    ArgumentMissing(&'static str),
    #[error("The path {} does not point to a file", .0.display())]
    NotAFile(PathBuf),
    #[error("The path {} does not point to a directory", .0.display())]
    NotADirectory(PathBuf),
    #[error(transparent)]
    ParseScmError(#[from] ScmError),
    #[error(transparent)]
    ParseIntError(#[from] std::num::ParseIntError),
    #[error("Cannot canonicalize the path")]
    CanonicalizePathError(#[source] std::io::Error),
    #[error(transparent)]
    BuildTargetError(#[from] BuildTargetError),
    #[error(transparent)]
    BuildTypeError(#[from] BuildTypeError),
}

fn main() -> Result<()> {
    // Load the command line arguments definition
    let cli_yaml = load_yaml!("project-build.yaml");
    let matches = App::from_yaml(cli_yaml)
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .get_matches();

    // Optional arguments
    let verbose = matches.occurrences_of("verbose");
    let mut strict = matches.is_present("strict");
    let cache_server = matches
        .value_of("cache-server")
        .ok_or(Error::ArgumentMissing("cache-server"))?;
    let builder_method = matches
        .value_of("builder-method")
        .ok_or(Error::ArgumentMissing("builder-method"))?;

    if builder_method == LEGACY_BM || builder_method == KOBOLDTOOLS_BM {
        strict = false;
    }

    let build_path_rel = matches
        .value_of("build-path")
        .ok_or(Error::ArgumentMissing("build-path"))?;
    let scm: Scm = matches
        .value_of("scm")
        .ok_or(Error::ArgumentMissing("scm"))
        .and_then(|s| s.parse().map_err(|e: ScmError| e.into()))
        .context("Cannot parse the scm argument")?;
    let build_type = matches
        .value_of("build-type")
        .ok_or(Error::ArgumentMissing("build-type"))
        .and_then(|bt| bt.parse().map_err(|e: BuildTypeError| e.into()))
        .context("Cannot parse the build-type argument")?;
    let build_number: usize = matches
        .value_of("build-number")
        .ok_or(Error::ArgumentMissing("build-number"))
        .and_then(|bn| bn.parse().map_err(|e: std::num::ParseIntError| e.into()))
        .context("Cannot parse the build-number argument")?;

    // Mandatory arguments
    let unity_path = matches
        .value_of("unity-path")
        .ok_or(Error::ArgumentMissing("unity-path"))
        .map(|up| PathBuf::from(up))
        .and_then(|up| {
            up.canonicalize()
                .map_err(|e| Error::CanonicalizePathError(e))
        })
        .and_then(|up| {
            if up.is_file() {
                Ok(up)
            } else {
                Err(Error::NotAFile(up))
            }
        })
        .context("Expected a path to a file for argument unity-path")?;
    let project_path = matches
        .value_of("project-path")
        .ok_or(Error::ArgumentMissing("project-path"))
        .map(|pp| PathBuf::from(pp))
        .and_then(|up| {
            up.canonicalize()
                .map_err(|e| Error::CanonicalizePathError(e))
        })
        .and_then(|up| {
            if up.is_dir() {
                Ok(up)
            } else {
                Err(Error::NotADirectory(up))
            }
        })
        .context("Expected a path to a directory for argument project-path")?;
    let target = matches
        .value_of("target")
        .ok_or(Error::ArgumentMissing("target"))
        .and_then(|bt| bt.parse().map_err(|e: BuildTargetError| e.into()))
        .context("Expected a target platform string for argument target")?;

    // Android-specific arguments
    let keystore = matches
        .value_of("keystore-path")
        .map(|kp| PathBuf::from(kp))
        .filter(|kp| kp.is_absolute() && kp.is_file())
        .and_then(|kp| matches.value_of("keystore-password").map(|kpw| (kp, kpw)))
        .and_then(|(kp, kpw)| matches.value_of("keyalias-name").map(|ka| (kp, kpw, ka)))
        .and_then(|(kp, kpw, ka)| {
            matches
                .value_of("keyalias-password")
                .map(|kapw| (kp, kpw, ka, kapw))
        })
        .map(|(kp, kpw, ka, kapw)| KeystoreCredentials::new(&kp, kpw, ka, kapw));

    // iOS-specific arguments
    let dev_team = matches.value_of("dev-team");
    let keychain_password = matches.value_of("keychain-password");
    let scheme = matches.value_of("scheme");

    // osx-specific arguments
    let signing_id = matches.value_of("signing-id");

    // Load the project configuration.
    let project = get_project(&project_path)?;

    // Determine the SCM location and the build path
    let scm_root = find_scm_root(scm, &project_path).expect("Cannot find the SCM root directory");
    let build_path = scm_root.join(build_path_rel);

    // Determine the app product path.
    let app_name = app_name(&project.name, &project_path);
    let product_path = product_path(
        &project.name,
        &project.version,
        target,
        &build_path,
        &project_path,
    );
    if verbose > 0 {
        println!("The product path is {}", product_path.display());
    }

    // Build the unity project
    build_unity(
        &unity_path,
        &cache_server,
        &project_path,
        &builder_method,
        build_type,
        target,
        build_number,
        &build_path,
        dev_team,
        keystore.as_ref(),
        strict,
        verbose,
    )
    .context("Cannot complete the unity build")?;

    // For iOS, we also need to invoke Xcode a few times -.-
    if target == BuildTarget::Ios {
        let scheme_internal = scheme.unwrap();

        let xcodeproj_path = product_path.join(format!("{}.xcodeproj", scheme_internal));
        let xcarchive_path = product_path.with_file_name(format!("{}.xcarchive", app_name));
        build_ios_xcarchive(
            &xcodeproj_path,
            &xcarchive_path,
            build_type,
            scheme_internal,
            keychain_password.unwrap(),
            verbose,
        )
        .context("Cannot complete the iOS build")?;

        let export_path = product_path.with_file_name(format!("{}-export", app_name));
        export_ios_ipa(
            ExportDestination::Export,
            &xcarchive_path,
            &export_path,
            dev_team.unwrap(),
            keychain_password.unwrap(),
            verbose,
        )
        .context("Cannot export the iOS build")?;
    }

    // Additional Signing required for OSX
    if target == BuildTarget::StandaloneOSX {
        let _ = sign_osx(&product_path, signing_id.unwrap());
    }

    Ok(())
}
