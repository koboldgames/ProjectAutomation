use anyhow::{Context, Result};
use clap::{load_yaml, App};
use project_automation::{
    build_target::{BuildTarget, Error as BuildTargetError},
    commands::{deploy_webgl, export_ios_ipa, notarize_mac_app},
    ios::export_destination::ExportDestination,
    scm::{Error as ScmError, Scm},
    utilities::{find_scm_root, get_project, product_path, app_name},
};
use std::path::PathBuf;
use thiserror::Error;

extern crate yaml_rust;
use yaml_rust::{YamlLoader};

#[derive(Debug, Error)]
enum Error {
    #[error("The {0} argument is missing")]
    ArgumentMissing(&'static str),
    #[error(transparent)]
    ParseScmError(#[from] ScmError),
    #[error("Cannot canonicalize the path")]
    CanonicalizePathError(#[source] std::io::Error),
    #[error("The path {} does not point to a directory", .0.display())]
    NotADirectory(PathBuf),
    #[error(transparent)]
    BuildTargetError(#[from] BuildTargetError),
}

fn main() -> Result<()> {
    // Load the command line arguments definition
    let cli_yaml = load_yaml!("project-deploy.yaml");
    let matches = App::from_yaml(cli_yaml)
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .get_matches();

    // Optional arguments
    let verbose = matches.occurrences_of("verbose");
    let build_path_rel = matches
        .value_of("build-path")
        .ok_or(Error::ArgumentMissing("build-path"))?;
    let scm: Scm = matches
        .value_of("scm")
        .ok_or(Error::ArgumentMissing("scm"))
        .and_then(|s| s.parse().map_err(|e: ScmError| e.into()))
        .context("Cannot parse the scm argument")?;

    // Mandatory arguments
    let project_path = matches
        .value_of("project-path")
        .ok_or(Error::ArgumentMissing("project-path"))
        .map(|pp| PathBuf::from(pp))
        .and_then(|up| {
            up.canonicalize()
                .map_err(|e| Error::CanonicalizePathError(e))
        })
        .and_then(|up| {
            if up.is_dir() {
                Ok(up)
            } else {
                Err(Error::NotADirectory(up))
            }
        })
        .context("Expected a path to a directory for argument project-path")?;
    let target = matches
        .value_of("target")
        .ok_or(Error::ArgumentMissing("target"))
        .and_then(|bt| bt.parse().map_err(|e: BuildTargetError| e.into()))
        .context("Expected a target platform string for argument target")?;

    // iOS-specific arguments
    let dev_team = matches.value_of("dev-team");
    let keychain_password = matches.value_of("keychain-password");

    // WebGL-specific arguments
    let sftp_host_path = matches.value_of("sftp-host-path");
    let sftp_username = matches.value_of("sftp-username");
    let sftp_password = matches.value_of("sftp-password");

    // OSX-specific arguments
    let apple_id = matches.value_of("apple-id");
    let app_pw = matches.value_of("app-pw");

    // Load the project configuration.
    let project = get_project(&project_path)?;

    // Determine the SCM location and the build path
    let scm_root = find_scm_root(scm, &project_path).expect("Cannot find the SCM root directory");
    let build_path = scm_root.join(build_path_rel);

    // Determine the app name.
    let app_name = app_name(&project.name, &project_path);
    if verbose > 0 {
        println!("The app name is {}", app_name);
    }

    // Determine the app product path.
    let product_path = product_path(
        &project.name,
        &project.version,
        target,
        &build_path,
        &project_path,
    );
    if verbose > 0 {
        println!("The product path is {}",product_path.display());
    }

    let r = match target {
        BuildTarget::Ios => {
            let xcarchive_path = product_path.with_file_name(format!("{}.xcarchive", app_name));
            let export_path = product_path.with_file_name(format!("{}-export", app_name));
            export_ios_ipa(
                ExportDestination::Upload,
                &xcarchive_path,
                &export_path,
                dev_team.unwrap(),
                keychain_password.unwrap(),
                verbose,
            )
        }
        BuildTarget::StandaloneOSX => notarize_mac_app(
            &product_path,
            apple_id.unwrap(),
            app_pw.unwrap(),
            dev_team.unwrap(),
            &project.version,
            target
        ),
        BuildTarget::WebGl => deploy_webgl(
            &product_path,
            sftp_host_path.unwrap(),
            sftp_username.unwrap(),
            sftp_password.unwrap(),
            verbose,
        ),
        _ => {
            eprintln!("Deploying on platform {} is not implemented", target);
            Ok(())
        }
    };
    r.expect("Could not deploy the product");

    Ok(())
}
