use anyhow::{Context, Result};
use clap::{load_yaml, App};
use project_automation::{
    build_target::{BuildTarget, Error as BuildTargetError},
    commands::{
        archive_android, archive_ios, archive_webgl, create_label_plasticscm, archive_osx, archive_other2
    },
    scm::{Error as ScmError, Scm},
    utilities::{find_scm_root, get_project, product_path, app_name},
};
use std::path::PathBuf;
use thiserror::Error;

extern crate yaml_rust;
use yaml_rust::{YamlLoader};

#[derive(Debug, Error)]
enum Error {
    #[error("The {0} argument is missing")]
    ArgumentMissing(&'static str),
    #[error("The path {} does not point to a directory", .0.display())]
    NotADirectory(PathBuf),
    #[error(transparent)]
    ParseScmError(#[from] ScmError),
    #[error(transparent)]
    ParseIntError(#[from] std::num::ParseIntError),
    #[error("Cannot canonicalize the path")]
    CanonicalizePathError(#[source] std::io::Error),
    #[error(transparent)]
    BuildTargetError(#[from] BuildTargetError),
}
fn main() -> Result<()> {
    // Load the command line arguments definition
    let cli_yaml = load_yaml!("project-archive.yaml");
    let matches = App::from_yaml(cli_yaml)
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .get_matches();

    // Optional arguments
    let verbose = matches.occurrences_of("verbose");
    let build_path_rel = matches
        .value_of("build-path")
        .ok_or(Error::ArgumentMissing("build-path"))?;
    let scm: Scm = matches
        .value_of("scm")
        .ok_or(Error::ArgumentMissing("scm"))
        .and_then(|s| s.parse().map_err(|e: ScmError| e.into()))
        .context("Cannot parse the scm argument")?;
    let scm_label = matches.value_of("scm-label");

    // Mandatory arguments
    let project_path = matches
        .value_of("project-path")
        .ok_or(Error::ArgumentMissing("project-path"))
        .map(|pp| PathBuf::from(pp))
        .and_then(|up| {
            up.canonicalize()
                .map_err(|e| Error::CanonicalizePathError(e))
        })
        .and_then(|up| {
            if up.is_dir() {
                Ok(up)
            } else {
                Err(Error::NotADirectory(up))
            }
        })
        .context("Expected a path to a directory for argument project-path")?;
    let target = matches
        .value_of("target")
        .ok_or(Error::ArgumentMissing("target"))
        .and_then(|bt| bt.parse().map_err(|e: BuildTargetError| e.into()))
        .context("Expected a target platform string for argument target")?;

    // iOS-specific arguments
    let scheme = matches.value_of("scheme");

    // Load the project configuration.
    let project = get_project(&project_path)?;

    // Determine the SCM location and the build path
    let scm_root = find_scm_root(scm, &project_path).expect("Cannot find the SCM root directory");
    let build_path = scm_root.join(build_path_rel);

    // Determine the app name.
    let app_name = app_name(&project.name, &project_path);
    if verbose > 0 {
        println!("The app name is {}",  app_name);
    }

    // Determine the app product path.
    let product_path = product_path(
        &project.name,
        &project.version,
        target,
        &build_path,
        &project_path,
    );
    if verbose > 0 {
        println!("The product path is {}", product_path.display());
    }

    // // Build the archive name
    // let archive_name = format!(
    //     "{}_{}_v{}",
    //     &project.name,
    //     target.to_short_str(),
    //     &project.version.public()
    // );
    // if verbose > 0 {
    //     println!("The archive name is {}", archive_name);
    // }

    // Archive the product.
    let r = match target {
        BuildTarget::Android => {
            archive_android(&project.version, target, &product_path)
        }
        BuildTarget::Ios => {
            let export_path;
            if &project.unity_major_version() >= &2020 && &project.unity_minor_version() >= &3 {
                export_path = product_path
                    .with_file_name(format!("{}-export", app_name))
                    .join(format!("{}.ipa", app_name));
            } else if &project.unity_major_version() >= &2020 && &project.unity_minor_version() >= &1 {
                // the following is not part of v7, because unity already changed the ipa naming behaviour
                let project_settings_string = std::fs::read_to_string(project_path.join("ProjectSettings/ProjectSettings.asset"))?;
                let project_settingss = YamlLoader::load_from_str(&project_settings_string)?;
                let project_settings = &project_settingss[0];
                let app_id_last =project_settings["PlayerSettings"]["applicationIdentifier"]["iPhone"].as_str().unwrap().split(".").last().unwrap();

                export_path = product_path
                    .with_file_name(format!("{}-export", app_name))
                    .join(format!("{}.ipa", app_id_last));
            } else {
                export_path = product_path
                    .with_file_name(format!("{}-export", app_name))
                    .join(format!("{}.ipa", scheme.unwrap()));
            }

            archive_ios(&project.version, target, &export_path)
        }
        BuildTarget::WebGl => archive_webgl(&product_path),
        BuildTarget::StandaloneOSX => archive_osx(&product_path, &project.version, target),
        BuildTarget::StandaloneLinux64 => archive_other2(&product_path, &project.version, target),
        _ => archive_other2(&product_path, &project.version, target),
    };
    r.expect("Could not archive the product");

    // Label the changeset
    if let Some(scm_label) = scm_label {
        if scm == Scm::PlasticScm {
            create_label_plasticscm(&scm_root, &scm_label, &project.version, target)
                .expect("Could not create the plasticscm label");
        }
    }

    Ok(())
}
