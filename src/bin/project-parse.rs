use anyhow::{Context, Result};
use clap::{load_yaml, App, AppSettings};
use project_automation::utilities::{get_project, Error as CommonError};
use std::path::PathBuf;

fn main() -> Result<()> {
    // Load and parse the command line arguments
    let cli_yaml = load_yaml!("project-parse.yaml");
    let matches = App::from_yaml(cli_yaml)
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .settings(&[AppSettings::DisableVersion])
        .get_matches();

    // Optional arguments
    let _verbose = matches.occurrences_of("verbose");

    // Mandatory arguments
    let project_path = matches
        .value_of("project-path")
        .ok_or(CommonError::ArgumentMissing("project-path"))
        .map(|p| PathBuf::from(p))
        .context("Cannot determine the unity project path")?;

    // Load the config
    let project = get_project(&project_path)?;

    if matches.is_present("name") {
        println!("{}", project.name);
    } else if matches.is_present("version") {
        println!("{}", project.version.internal());
    } else if matches.is_present("unity-version-long") {
        println!("{}", project.unity_version);
    } else if matches.is_present("unity-version-short") {
        println!("{}", project.unity_version_short());
    } else if matches.is_present("targets") {
        let targets = project.build_targets
            .iter()
            .map(|t| t.target.to_str())
            .collect::<Vec<_>>();

        println!("{}", targets.join(" "));
    } else if matches.is_present("all") {
        println!("{:#?}", project);
    }

    Ok(())
}
