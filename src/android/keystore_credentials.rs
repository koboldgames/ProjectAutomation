use std::path::Path;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct KeystoreCredentials {
    pub path: String,
    pub password: String,
    pub alias: String,
    pub alias_password: String,
}

impl KeystoreCredentials {
    pub fn new(path: &Path, password: &str, alias: &str, alias_password: &str) -> Self {
        KeystoreCredentials {
            path: format!("{}", path.display()),
            password: password.into(),
            alias: alias.into(),
            alias_password: alias_password.into(),
        }
    }
}
